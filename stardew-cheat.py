#!/usr/bin/env python3
import argparse
import os.path
import shutil
import sys
import xml.dom.minidom


def cheat_bag(xml_obj):
    player = xml_obj.getElementsByTagName("player")[0]

    # Change content of <maxItems> tag
    max_items = player.getElementsByTagName("maxItems")[0]
    max_items.removeChild(max_items.firstChild)
    max_items.appendChild(xml_obj.createTextNode("36"))

    print("maxItems will be set to 36.")
    return xml_obj

def cheat_item(xml_obj):
    player = xml_obj.getElementsByTagName("player")[0]
    player_items = player.getElementsByTagName("items")[0]
    player_first_item = player_items.getElementsByTagName("Item")[0]

    # Change any <Stack> or <stack> tag content to "999"
    for node in player_first_item.childNodes:
        if node.nodeName == "Stack" or node.nodeName == "stack":
            node.removeChild(node.firstChild)
            node.appendChild(xml_obj.createTextNode("999"))

    item_name_tag = player_first_item.getElementsByTagName("name")[0]
    item_name_value = item_name_tag.firstChild.nodeValue
    print("{} will be set to 999.".format(item_name_value))
    return xml_obj


def cheat_money(xml_obj):
    player = xml_obj.getElementsByTagName("player")[0]

    # Change content of <money> tag
    player_money = player.getElementsByTagName("money")[0]
    player_money_value = player_money.firstChild
    player_money.removeChild(player_money_value)
    player_money_value_new = xml_obj.createTextNode("99999999")
    player_money.appendChild(player_money_value_new)

    print("Money will be set to 99999999.")
    return xml_obj


def cheat_rain(xml_obj):
    # Change content of <isRaining> tag
    rain = xml_obj.getElementsByTagName("isRaining")[0]
    rain.removeChild(rain.firstChild)
    rain.appendChild(xml_obj.createTextNode("true"))

    # Set other weather tags content to 'false'
    debris = xml_obj.getElementsByTagName("isDebrisWeather")[0]
    debris.removeChild(debris.firstChild)
    debris.appendChild(xml_obj.createTextNode("false"))

    lightning = xml_obj.getElementsByTagName("isLightning")[0]
    lightning.removeChild(lightning.firstChild)
    lightning.appendChild(xml_obj.createTextNode("false"))

    snow = xml_obj.getElementsByTagName("isSnowing")[0]
    snow.removeChild(snow.firstChild)
    snow.appendChild(xml_obj.createTextNode("false"))

    print("isRaining will be set to 'true'.")
    return xml_obj


def cheat_water(xml_obj):
    player = xml_obj.getElementsByTagName("player")[0]

    # Change content of <hasWateringCanEnchantment> tag
    water_enchant = player.getElementsByTagName("hasWateringCanEnchantment")[0]
    water_enchant.removeChild(water_enchant.firstChild)
    water_enchant.appendChild(xml_obj.createTextNode("true"))

    print("hasWateringCanEnchantment will be set to 'true'.")
    return xml_obj


def goto_savefile_dir(xml_file_path):
    savefile_dir = os.path.dirname(os.path.abspath(xml_file_path))
    savefile_name = os.path.basename(os.path.abspath(xml_file_path))

    # Go to the save file directory and create a backup the first time.
    os.chdir(savefile_dir)
    if not os.path.exists(savefile_name + ".backup"):
        shutil.copy(savefile_name, savefile_name + ".backup")
        print("Backup created:")
        print(os.path.abspath(savefile_name + ".backup"))
        print("")


def write_xml_out(xml_obj, xml_file_path):
    # Ask confirmation before writing changes
    print("Are you sure? y/n")
    if str(input()) == "y":
        print("Wait...")
        with open(xml_file_path, "w") as f:
            f.write(xml_obj.toxml())
        print("Done!")
    else:
        print("Canceled.")


def get_cli_parser():
    # CLI argument parsing settings
    parser = argparse.ArgumentParser(
        description='Stardew Valley save file editor',
        usage="{} [OPTION] [SAVE FILE PATH]".format(sys.argv[0]))

    parser.add_argument(
        "-b",
        "--bag",
        type=str,
        metavar="",
        help="enable 36 slots in the inventory")

    parser.add_argument(
        "-i",
        "--item",
        type=str,
        metavar="",
        help="set quantity of first item in inventory to 999")

    parser.add_argument(
        "-m",
        "--money",
        type=str,
        metavar="",
        help="set quantity of money to 99999999")

    parser.add_argument(
        "-r",
        "--rain",
        type=str,
        metavar="",
        help="force rain")

    parser.add_argument(
        "-w",
        "--water",
        type=str,
        metavar="",
        help="infinite water in watering can")

    return parser


if __name__ == "__main__":
    # Argument handling
    args = get_cli_parser().parse_args()
    avail_args = {
        args.bag: cheat_bag,
        args.item: cheat_item,
        args.money: cheat_money,
        args.rain: cheat_rain,
        args.water: cheat_water,
    }

    for arg, cheat_func in avail_args.items():
        if arg is not None:
            savefile_path = arg
            selected_cheat = cheat_func
            break

    goto_savefile_dir(savefile_path)
    savefile_xml_obj = xml.dom.minidom.parse(savefile_path)

    new_xml = selected_cheat(savefile_xml_obj)
    write_xml_out(new_xml, savefile_path)
