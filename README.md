# Stardew Valley save file editor.

Stardew-Cheat is a **command line python script** to modify a Stardew Valley save file.

It should work on Windows, Mac and Linux has long as you have **python3** installed. 
However, it is primarily intended for Linux users (or anyone familiar with a CLI).

## INSTALL

First, make sure you have **python3** installed.
Then, use the following commands to download the script and install it.

    wget "https://gitlab.com/mgriseri/stardew-cheat/raw/master/stardew-cheat.py"
    chmod +x stardew-cheat.py
    sudo mv stardew-cheat.py /usr/local/bin/stardew-cheat

> You can uninstall it by removing the file with `sudo rm /usr/local/bin/stardew-cheat`

## USAGE

When the game is **NOT running**, use from the command line.

    stardew-cheat [OPTION] [SAVE FILE PATH]
    
## OPTIONS

    -b , --bag      enable 36 slots in the inventory
    -i , --item     set quantity of first item in inventory to 999
    -m , --money    set quantity of money to 99999999
    -r , --rain     force rain
    -w , --water    infinite water in watering can        
      
## EXAMPLES

    stardew-cheat --help
    stardew-cheat --money ~/.config/StardewValley/Saves/Name_000000000/Name_000000000
    stardew-cheat --item ~/.config/StardewValley/Saves/Name_000000000/Name_000000000
    
## NOTES

Your Stardew Valley save file is probably located in `~/.config/StardewValley`.

More info there: https://stardewvalleywiki.com/Saves

## LICENSE


GNU GENERAL PUBLIC LICENSE:

[https://www.gnu.org/licenses/gpl.html](https://www.gnu.org/licenses/gpl.html)

## CONTRIBUTE

If you want to adapt the source code, it is under the GPL, you're free to do so. 

If this script has been helpful to you, feel free to [send a donation](https://www.paypal.me/mgriseri).